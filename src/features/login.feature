Feature: Test registration page
    As a new user
    I want to validate registration fields

    Scenario: [Jobla][Home] Check home page
        Given I open the home page
        When I accept cookies
        And I switch language to ukr
        Then I expect to be on home page

    Scenario: [Jobla][Signup] Check registration page
        When I click on registration button
        Then I expect to be on signup page
        And inputEmail field is present
        And inputPassword field is present

    Scenario: [Jobla][Signup] Check submit with empty fields
        When I submit signup form
        Then account is not created
        And errMandatoryField message is shown

    Scenario: [Jobla][Signup] Check submit with invalid email and no password
        When I enter invalid email "go.ua"
        And I submit signup form
        Then errValidEmail message is shown
        And account is not created
        
        #clear to empty value
        When I clear inputEmail field
        And I submit signup form
        Then errValidEmail message is shown
        
        #valid=>invalid
        When I enter valid email "go@com.ua"
        Then errValidEmail message is not shown
        
        #invalid    
        When I clear inputEmail field
        And I enter invalid email "go@ua"
        Then errValidEmail message is shown
        When I submit signup form
        And I clear inputEmail field

    Scenario: [Jobla][Signup] Check submit with valid email and no password
        When I enter valid email "go@com.ua"
        Then errValidEmail message is not shown
        When I submit signup form
        Then account is not created

    Scenario: [Jobla][Signup] Check password field validation with sign
        When I enter invalid password "!"
        Then 1 checkbox is inactive
        And 2 checkbox is inactive
        And 3 checkbox is inactive
        When I submit signup form
        Then account is not created
        When I clear inputPassword field

    Scenario: [Jobla][Signup] Check password field validation with sign/digit
        When I enter invalid password "!1"
        Then 3 checkbox is active
        When I submit signup form
        Then account is not created
        When I clear inputPassword field

    Scenario: [Jobla][Signup] Check password field validation with sign/digit/letter
        When I enter invalid password "!1a"
        Then 2 checkbox is active
        And 3 checkbox is active
        When I submit signup form
        Then account is not created
        When I clear inputPassword field

    Scenario: [Jobla][Signup] Check password field length validation
        When I enter invalid password "!1a45678901234567890123456789012345678901"
        Then 1 checkbox is inactive
        And 2 checkbox is active
        And 3 checkbox is active
        When I clear inputPassword field
        And I enter valid password "!1a4567890123456789012345678901234567890"
        Then 1 checkbox is active

    Scenario: [Jobla][Signup] Check password field with valid value
        When I clear inputPassword field
        And I enter valid password "!1a45678"
        Then 1 checkbox is active
        And 2 checkbox is active
        And 3 checkbox is active

    Scenario: [Jobla][Signup] Check submit with valid password and no email   
        When I refresh browser tab
        And I enter valid password "!1a45678"
        And I submit signup form
        Then account is not created

    Scenario: [Jobla][Signup] Check submit with valid email and password
        When I enter valid email "go@com.ua"
        And I submit signup form

