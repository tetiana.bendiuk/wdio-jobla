const Page = require('./page.js');

class SignupPage extends Page {

    get formSignup() { return $("//*[@class='job-signUp-form']") }
    get btnSubmit() { return $("//button[@type='submit']") }
    get inputEmail() { return $("//input[@type='email']") }
    get inputPassword() { return $("//input[@type='password']") }
    get btnEye() { return $("//div[@class='job-input-override-bootstrap__append-slot' and descendant::*[@data-icon='eye-slash']]") }
    get msgCheckMailboxUkr() { return $("//*[contains(text(),'Перевір пошту...')]") }
    get errMandatoryField() { return $(`//div[@class="form-element-has-error__text" and contains(text(),"Це поле обов'язкове")]`) }
    get errValidEmail() { return $("//div[@class='form-element-has-error__text' and contains(text(),'Будь ласка, введи дійсну електронну адресу')]") }
    get checkPassLength() { return $("//*[@class='hint__item hint__item--hasIcon' and contains(text(),'Містить від 8 до 40 символів')]") }
    get checkPassLetters() { return $("//*[@class='hint__item hint__item--hasIcon' and contains(text(),'Містить літери')]") }
    get checkPassDigits() { return $("//*[@class='hint__item hint__item--hasIcon' and contains(text(),'Містить цифри')]") }
    checkPassActiveOrder(order) { return $(`//li[contains(@class,'hint__item')][position()='${order}']//ancestor::*[contains(@class,'hint__item--hasIcon')]`) }

    signup() {
        this.btnSubmit.click()
    }

    setEmail(emailValue) {
        this.inputEmail.setValue(emailValue).pause(300)
    }
    setPassword(passwordValue) {
        this.inputPassword.setValue(passwordValue).pause(300)
    }

    checkField(fieldName) {
        this[fieldName].waitForVisible(3000, false)
    }
    clearField(fieldName) {
        this[fieldName].clearElement()
    }
    clickField(fieldName) {
        this[fieldName].click()
    }


}

module.exports = new SignupPage();